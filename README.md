# PiRelay #

Based on [Web Interface for Raspberry Pi](http://www.instructables.com/id/Simple-and-intuitive-web-interface-for-your-Raspbe/) tutorial

This responsive web app controls an 8 channel relay via the Raspberry Pi.

### How do I get set up? ###

* Requirements (Apache, PHP, GPIO library called [Wiring Pi](http://wiringpi.com/) made by Gordon Henderson)
* See [Web Interface for Raspberry Pi](http://www.instructables.com/id/Simple-and-intuitive-web-interface-for-your-Raspbe/) for hardware set up. However, I didn't use the same pins configuration in the tutorial. Pins configurations must be changed in `index.php`, `gpio.php`, and `script.js` files. The pins are stored in arrays called `channels` in respect to the channels on the relay board (i.e. `channels[1]` is the pin connected to channel 2 of the relay board). Wiring Pi uses a different map of the RPi GPIO pins. See more [here](https://projects.drogon.net/raspberry-pi/wiringpi/pins/).
* Deployment instructions: Simply place all files (including the `data` folder) into `/var/www/` or appropriate root directory of the server.
