<!DOCTYPE html>
<html>
<head>
  <title>RPi Controller</title>
<link rel="stylesheet" type="text/css" href="style.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script>
function startTime() {
    var today=new Date();
    var t = setTimeout(function(){startTime()},500);
    document.getElementById('current_time').innerHTML = today;
}
</script>
</head>
<body onload="startTime()">
<div id="current_time"></div>
<?php
  $check_temp = "/opt/vc/bin/vcgencmd measure_temp";
  $temp = shell_exec($check_temp);
  echo "<div id='rpi_temp'>$temp</div>"
?>
<div id="buttons">
<?php
  $channels = array(15, 16, 1, 4, 5, 6, 10, 11);

  for ($i=0;$i<8;$i++) {
    $value = shell_exec("gpio read $channels[$i]");
    if ($value == 1) {
      echo ("<div class='button'><img id='button_$i' src='data/img/red/red.jpg' onclick='change_pin ($i);'/>");
      echo ("<p>$i</p></div>");
    }
    else {
      echo ("<div class='button'><img id='button_$i' src='data/img/green/green.jpg' onclick='change_pin ($i);'/>");
      echo ("<p>$i</p></div>");
    }
  }
?>
</div>

<script src="script.js"></script>
</body>
</html>
